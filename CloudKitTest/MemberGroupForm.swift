//
//  MemberGroupForm.swift
//  CloudKitTest
//
//  Created by Keith Weiss on 6/21/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI

struct MemberGroupForm : View {
    @EnvironmentObject private var memberGroupService: MemberGroupsService
    @Binding var memberGroup: MemberGroup
    @State private var showGroupEntryView: Bool = false
    
    var body: some View {
            Form {
                Button(action: {
                    withAnimation {
                        self.showGroupEntryView.toggle()
                    }
                }) {
                    HStack {
                        Spacer()
                        Text("Add a Member Group").foregroundColor(.orange)
                        Image(systemName: "plus.circle.fill")
                            .imageScale(.large)
                            .rotationEffect(.degrees(showGroupEntryView ? 45 : 0))
                            .scaleEffect(showGroupEntryView ? 1.5 : 1)
                            .padding()
                            .foregroundColor(.orange)
                    }
                }
                .accentColor(.orange)
                
                if showGroupEntryView {
                    HStack {
                        Text("Title").font(.subheadline)
                        Divider()
                        TextField("The name of the group", text: $memberGroup.title)
                    }
                    HStack {
                        Text("Description").font(.subheadline)
                        Divider()
                        TextField("notes about the group", text: $memberGroup.details)
                    }
                    HStack {
                        Button(action: $memberGroup.value.save) {
                            HStack {
                                Text("Save to the Cloud!")
                                Image(systemName: "icloud.and.arrow.up.fill")
                                    .imageScale(.medium)
                                    .accessibility(label: Text("Save to Cloud"))
                                }
                                .padding(8.0)
                                .frame(height: 44)
                            }
                            .accentColor(.orange)
                            .border(Color.orange, width: 1.0, cornerRadius: 9.0)
                            .padding(8.0)
                    }
                }
                
                Picker(selection: /*@START_MENU_TOKEN@*/.constant(1)/*@END_MENU_TOKEN@*/, label: Text("Pick a Group")) {
                    Text("My Group").tag(1)
                    Text("His Group").tag(2)
                }
                Spacer()
            }
            .navigationBarTitle(Text("Member Groups"))
    }
}

#if DEBUG
struct MemberGroupForm_Previews : PreviewProvider {
    static var previews: some View {
        NavigationView {
            MemberGroupForm(memberGroup: .constant(MemberGroup(title: ""))).environmentObject(MemberGroupsService())
        }
    }
}
#endif
