//
//  DataController.swift
//  CloudKitTest
//
//  Created by Keith Weiss on 6/19/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import Combine

let memberPublisher = URLSession.shared.dataTaskPublisher(for: URL(string: "https://watchdogssignup.herokuapp.com/members/")!)
    .map({ (response: (data: Data, response: URLResponse)) -> Data in
        return response.data
    })
    .retry(1)
    .decode(type: [Member].self, decoder: JSONDecoder())
    .catch({error in
        return Just(Member.placeholder)
    })
    .receive(on: RunLoop.main)


final class MembersService: BindableObject {
    var canceller: Cancellable?
    let didChange = PassthroughSubject<MembersService, Never>()
    
    var hasMembers: Bool {
        return !members.isEmpty
    }
    
    var showFavoritesOnly = false {
        didSet {
            didChange.send(self)
        }
    }
    
    private var _searchTerm: String = ""
    var searchTerm: String = "" {
        didSet {
            if _searchTerm != searchTerm {
                fetch()
            }
            _searchTerm = searchTerm
        }
    }
    
    var members: [Member] = Member.placeholder {
        didSet {
            members = members.filter({ $0.name.starts(with: searchTerm)}) .sorted(by: { $0.name < $1.name})
            didChange.send(self)
            canceller?.cancel()
        }
    }
    
    init() {
        fetch()
    }
    
    func fetch() {
        canceller = memberPublisher
            .assign(to: \.members, on: self)
    }
    
    func clearTerm() {
        searchTerm = ""
    }
}


final class MemberGroupsService: BindableObject {
//    var canceller: Cancellable?
    let didChange = PassthroughSubject<MemberGroupsService, Never>()
    
    var hasMemberGroups: Bool {
        return !memberGroups.isEmpty
    }
    
    var title: String = "" {
        didSet {
            print("----> Added a title for MembershipGroup: \(title)")
            didChange.send(self)
        }
    }
    
    var memberGroups: [MemberGroup] = MemberGroup.placeholder {
        didSet {
            memberGroups = memberGroups.sorted(by: { $0.title < $1.title})
            didChange.send(self)
//            canceller?.cancel()
        }
    }
    
    init() {
        fetch()
    }
    
    func fetch() {
//        canceller = memberPublisher
//            .assign(to: \.members, on: self)
    }
}

