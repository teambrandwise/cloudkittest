//
//  MemberDetail.swift
//  CloudKitTest
//
//  Created by Keith Weiss on 6/19/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import MessageUI

struct MemberDetail: View {
    var member: Member
    
    var body: some View {
        Form {
            Section(header: Text("Member Info")) {
                HStack {
                    Text("Full name: ").font(.subheadline)
                    Divider()
                    Text(verbatim: member.name)
                }
                HStack {
                    Text("Email address: ").font(.subheadline)
                    Divider()
                    Text(verbatim: member.email)
                }
                HStack {
                    Text("ID: ").font(.subheadline)
                    Divider()
                    Text(verbatim: String(member.id))
                }
            }
            Section {
                Button(action: sendEmail) {
                    HStack {
                        Text("Send to: ")
                        Text(verbatim: member.email)
                        Image(systemName: "envelope.fill")
                            .imageScale(.medium)
                            .accessibility(label: Text("Email Address"))
                        }
                        .padding(8.0)
                        .frame(height: 44)
                    }
                    .accentColor(.orange)
                    .border(Color.orange, width: 1.0, cornerRadius: 9.0)
                    .padding(8.0)
            }
            }
            .navigationBarTitle(Text("Member Details"))
        // TODO: - Find out if onDisappear works with future updates to XCode 11.
    }
    
    func sendEmail() {
        guard MFMailComposeViewController.canSendMail() else { return }
        
        UIApplication.sendEmail(to: member)
    }
}

#if DEBUG
struct MemberDetail_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            NavigationView {
                MemberDetail(member: Member.default)
            }
            .accentColor(.orange)
        }
    }
}
#endif
