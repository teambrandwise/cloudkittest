//
//  UIApplication+Extension.swift
//  CloudKitTest
//
//  Created by Keith Weiss on 6/19/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import UIKit
import MessageUI

extension UIApplication {
    static func sendEmail(to recipient: Member) {
        guard let sceneDelegate = UIApplication.shared.openSessions.first?.scene?.delegate as? SceneDelegate,
            let rootController = sceneDelegate.window?.rootViewController else { return }
        
        let mail = MFMailComposeViewController()
        mail.navigationBar.tintColor = UIColor.systemOrange
        mail.setToRecipients([recipient.email])
        mail.setSubject("I have a question")
        mail.setMessageBody("<p>Can you help me, please!</p>", isHTML: true)
        mail.mailComposeDelegate = UIApplication.shared.delegate as! AppDelegate
        rootController.present(mail, animated: true, completion: nil)
    }
}
