//
//  MemberList.swift
//  CloudKitTest
//
//  Created by Keith Weiss on 6/19/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI

struct MemberList : View {
    @EnvironmentObject private var memberService: MembersService
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading, spacing: 20) {
                HStack(alignment: .center, spacing: 8) {
//                    Text("Find a Member")
//                    Divider()
                    Image(systemName: "magnifyingglass")
                    TextField("name starts with…", text: $memberService.searchTerm)
                    if $memberService.searchTerm.value != "" {
                        Button(action: memberService.clearTerm) {
                            Image(systemName: "x.circle.fill")
                                .padding(.trailing, 8)
                                .imageScale(.large)
                        }
                        .accentColor(.secondary)
                    }
                }
                .frame(width: nil, height: 38, alignment: .topLeading)
                    .padding([.leading,.top], 16)
                    .border(Color.secondary, width: 1.0, cornerRadius: 9.0)
                .padding()
                
                if memberService.members.count > 0 {
                    List {
                        ForEach(memberService.members) { member in
                            NavigationLink(destination: MemberDetail(member: member)) {
                                MemberRow(member: member)
                                    .onDisappear(perform: {
                                        MembersService().fetch()
                                    })
                            }
                        }
                    }
                    .navigationBarTitle(Text("Members"), displayMode: .large)
                        .navigationBarItems(trailing:
                            HStack {
                                PresentationLink(destination: MemberGroupForm(memberGroup: .constant(MemberGroup(title: ""))).environmentObject(MemberGroupsService()),
                                                 label: {
                                                    Image(systemName: "person.2.square.stack.fill")
                                                        .imageScale(.large)
                                                        .accessibility(label: Text("Group for Members"))
                                                        .padding()
                                })
                                
                                Button(action: memberService.fetch) {
                                    Image(systemName: "arrow.clockwise")
                                        .imageScale(.large)
                                }
                            }
                    )
                        .accentColor(.orange)
                        .navigationBarTitle(Text("Members"), displayMode: .large)
                }
                else {
                    Spacer()
                    
                    Text("Fetching members from the cloud…")
                        .navigationBarTitle(Text("Members"), displayMode: .large).accentColor(.orange)
                    
                    Spacer()
                }
            }
        }
    }
}


#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        MemberList().environmentObject(MembersService())
    }
}
#endif
