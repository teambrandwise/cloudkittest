//
//  MemberGroup.swift
//  CloudKitTest
//
//  Created by Keith Weiss on 6/21/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import Foundation
import SwiftUI
import CloudKit

struct MemberGroup: Hashable, Identifiable, CustomStringConvertible {
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case details
    }
    
    let id: UUID
    var title: String {
        didSet {
            print("---> Title: \(title)")
        }
    }
    var details: String
    
    init(title: String) {
        self.init(UUID(),title,"")
    }
    
    init(_ id: UUID? = UUID(),_ title: String,_ details: String? = "") {
        self.id = id!
        self.title = title
        self.details = details!
    }
    
    private var _cloudKitRecord: CKRecord?
    var cloudKitRecord: CKRecord {
        guard _cloudKitRecord == nil else { return _cloudKitRecord! }
        
        let recordID = CKRecord.ID(recordName: self.id.uuidString)
        let _cloudKitRecord = CKRecord(recordType: "MemberGroup", recordID: recordID)
        _cloudKitRecord[CodingKeys.title.rawValue] = self.title as NSString
        _cloudKitRecord[CodingKeys.details.rawValue] = self.details as NSString
        
        return _cloudKitRecord
    }
    
    #if DEBUG
    static var placeholder = [`default`]
    #else
    static var placeholder = []
    #endif
    
    static var `default` = MemberGroup(UUID(), "My First Group", "This member group contains familiar members.")
    var description: String {
        return "\n  MemberGroup:\n    title: \(title)\n   title: \(details)\n      ID: \(id)\n"
    }
    
    func save() {
        print(self)
        (UIApplication.shared.delegate as! AppDelegate).save(MemberGroup.default.cloudKitRecord)
    }
}

// MARK: - Decodable

extension MemberGroup: Decodable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decodeIfPresent(UUID.self, forKey: .id) ?? UUID()
        self.title = try container.decode(String.self, forKey: .title)
        self.details = try container.decodeIfPresent(String.self, forKey: .details) ?? ""
    }
}


// MARK: - Encodeable
extension MemberGroup: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(id, forKey: .title)
        try container.encode(id, forKey: .details)
    }
}
