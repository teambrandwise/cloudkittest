//
//  Member.swift
//  CloudKitTest
//
//  Created by Keith Weiss on 6/21/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import Foundation
import SwiftUI

struct Member: Hashable, Codable, Identifiable, CustomStringConvertible {
    private enum codingKeys: String, Codable {
        case id
        case email
        case name
    }
    
    
    let id: Int64
    let email: String
    let name: String
    
    #if DEBUG
    static var placeholder = [`default`]
    #else
    static var placeholder = []
    #endif
    
    static var `default` = Member(id: 0, email: "admin@admin.com", name: "admin")
    var description: String {
        return "\n  Member:\n    name: \(name)\n   email: \(email)\n      ID: \(id)\n"
    }
}
