//
//  MemberRow.swift
//  CloudKitTest
//
//  Created by Keith Weiss on 6/19/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI

struct MemberRow: View {
    var member: Member
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .center, spacing: 8) {
                Image(systemName: "person.crop.circle")
                    .imageScale(.medium)
                    .accessibility(label: Text("Member Name"))
                Text(verbatim: member.name)
                    .font(.headline)
            }
            HStack(alignment: .center, spacing: 8) {
                Image(systemName: "envelope.fill")
                    .imageScale(.medium)
                    .foregroundColor(.secondary)
                    .accessibility(label: Text("Email Address"))
                Text(verbatim: member.email)
                    .font(.subheadline)
                    .color(.secondary)
            }
        }
    }
}

#if DEBUG
struct MemberRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            MemberRow(member: MembersService().members[0])
            }
            .previewLayout(.fixed(width: 600, height: 70))
    }
}
#endif
